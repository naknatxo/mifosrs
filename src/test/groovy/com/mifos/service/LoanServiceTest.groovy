package com.mifos.service

import org.junit.After
import org.junit.Before
import org.junit.Ignore;
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.context.ApplicationContext
import org.springframework.test.annotation.Rollback
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration
import org.springframework.transaction.annotation.Transactional

import com.mifos.domain.LoanRepaymentSchedule;
import com.mifos.repository.LoanRepaymentScheduleRepository;
import com.mifos.config.Application;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
		classes = Application
)
@ActiveProfiles(["test,hsql-local,populate"])
class LoanServiceTest {

	
	@Autowired
	private ApplicationContext applicationContext
	
	@Autowired
	private LoanService service
	
	@Before
	void setUp() {
	}

	@After
	void tearDown(){
	}

	@Test
	void assert_context_and_repository() {
		assert applicationContext
		assert service
	}
	
	@Test
	void do_all_staff_should_work(){
		Integer transformed = service.filterAndTransform(1L)
		println "Transformer:: ${transformed}"
	}
}

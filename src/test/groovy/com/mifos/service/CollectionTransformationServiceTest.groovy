package com.mifos.service

import org.junit.After
import org.junit.Before
import org.junit.Ignore;
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.context.ApplicationContext
import org.springframework.test.annotation.Rollback
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration
import org.springframework.transaction.annotation.Transactional

import com.mifos.domain.LoanRepaymentSchedule;
import com.mifos.repository.LoanRepaymentScheduleRepository;
import com.mifos.config.Application;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
		classes = Application
)
@ActiveProfiles(["test,hsql-local,populate"])
class CollectionTransformationServiceTest {

	@Autowired
	private ApplicationContext applicationContext
	
	@Autowired
	private LoanRepaymentScheduleService repository
	
	@Autowired
	private CollectionFilterService collectionFilterService
	
	@Autowired
	private CollectionTransformationService service
	
	@Before
	void setUp() {
	}

	@After
	void tearDown(){
	}

	@Test
	void assert_context_and_repository() {
		assert applicationContext
		assert repository
		assert collectionFilterService
		assert service
	}

	@Test
	void find_by_loan_should_transfor_collection(){
		println '**************** find_by_loan_should_transfor_collection:'
		
		Collection<LoanRepaymentSchedule> col = repository.findByLoanIdOrderByFromdate(1L)
		
		assert col && col.size() > 1 && col.size() == 4
		
		Collection<LoanRepaymentSchedule> result = collectionFilterService.filter(col)
		
		assert result.size() == 3
	
		Collection<LoanRepaymentSchedule> transformed = service.transform(result)
		println "Transformer:: "
		transformed.each {
			println it.toString()
		}
	}
}

package com.mifos.service

import org.junit.After
import org.junit.Before
import org.junit.Ignore;
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.context.ApplicationContext
import org.springframework.test.annotation.Rollback
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration
import org.springframework.transaction.annotation.Transactional

import com.mifos.domain.LoanRepaymentSchedule;
import com.mifos.repository.LoanRepaymentScheduleRepository;
import com.mifos.config.Application;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
		classes = Application
)
@ActiveProfiles(["test,hsql-local,populate"])
class LoanRepaymentScheduleServiceTest {

	@Autowired
	private ApplicationContext applicationContext
	
	@Autowired
	private LoanRepaymentScheduleService service
	
	@Before
	void setUp() {
	}

	@After
	void tearDown(){
	}

	@Test
	void assert_context_and_repository() {
		assert applicationContext
		assert service
	}
	
	@Ignore("ok")
	@Test
	void find_by_loanId_should_return_collection(){
		println '**************** find_by_loan_should_get_collection:'
		
		Collection<LoanRepaymentSchedule> col = service.findByLoanIdOrderByFromdate(1L)
		assert col && col.size() > 1
	}
	
	@Test
	void find_by_loanId_should_return_empty(){
		
		println '**************** find_by_loanId_should_return_empty:'
		
		Collection<LoanRepaymentSchedule> col = service.findByLoanIdOrderByFromdate(99L)
		assert col == [] && col.size() == 0
	}
	
	@Ignore("ok")
	@Test
	void save_null_should_return_empty_list(){
		println '**************** save_null_should_return_empty_list:'
		
		Collection<LoanRepaymentSchedule> col = service.save(null)
		assert col == [] && col.size() == 0
	}
	
	@Ignore("ok")
	@Test
	void save_empty_list_should_return_empty_list(){
		println '**************** save_empty_list_should_return_empty_list:'
		
		Collection<LoanRepaymentSchedule> col = service.save(Collections.EMPTY_LIST)
		assert col == [] && col.size() == 0
	}
	
}

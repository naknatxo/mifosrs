package com.mifos.repository

import java.util.Collection;

import javax.sql.DataSource;

import com.mifos.domain.LoanRepaymentSchedule;

import org.junit.After
import org.junit.Before
import org.junit.Ignore;
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.context.ApplicationContext
import org.springframework.test.annotation.Rollback
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration
import org.springframework.transaction.annotation.Transactional

import com.mifos.config.Application;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
		classes = Application
)
@ActiveProfiles(["test,hsql-local,populate"])
class LoanRepaymentScheduleRepositoryTest {

	@Autowired
	private ApplicationContext applicationContext
	
	@Autowired
	private DataSource dataSource
	
	@Autowired
	private LoanRepaymentScheduleRepository repository
	
	@Before
	void setUp() {
	}

	@After
	void tearDown(){
	}

	@Test
	void assert_context_and_repository() {
		println '*********** assert_context_and_repository:'
		assert applicationContext
		assert repository
		assert dataSource
		println dataSource.getProperties().toString()
	}
	
	@Test
	void find_by_loan_should_get_collection(){
		println '**************** find_by_loan_should_get_collection:'
		
		Collection<LoanRepaymentSchedule> col = repository.findByLoanIdOrderByFromdate(1L)
		col.each {
			println it.toString()
		}
		println 'col.size():: '+col.size()
		assert col && col.size() > 1
	}
	
	
	@Ignore("Checked Ok")
	@Test
	void update_one_must_be_done_and_respect_all_database_fields(){
		println '**************** update_one_must_be_done_and_respect_all_database_fields:'
		
		Collection<LoanRepaymentSchedule> col = repository.findByLoanIdOrderByFromdate(42L)
		
		col.each {
			println it.toString()
			if (it.id == 2940L){
				println 'Modificating:: ' + it.toString()
				it.installment = 69
				repository.save(it)
			}
		}
		
		assert col && col.size() > 1
	}
}

package com.mifos.repository

import org.springframework.data.jpa.repository.JpaRepository;

import com.mifos.domain.Loan;

interface LoanRepository extends JpaRepository<Loan, Long>{

}

package com.mifos.repository

import org.springframework.data.jpa.repository.JpaRepository

import com.mifos.domain.LoanRepaymentSchedule;

interface LoanRepaymentScheduleRepository extends JpaRepository<LoanRepaymentSchedule, Long>{
	
	Collection<LoanRepaymentSchedule> findByLoanIdOrderByFromdate(Long loanId)
}

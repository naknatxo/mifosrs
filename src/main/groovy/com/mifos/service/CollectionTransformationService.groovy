package com.mifos.service

import java.util.Collection;

interface CollectionTransformationService<T> {

	/**
	 * Returns de same collection but the objects will be changed accord to bussines logic
	 * @param col
	 * @return
	 */
	Collection<T> transform(Collection<T> col)	
}

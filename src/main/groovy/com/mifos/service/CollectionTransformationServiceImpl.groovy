package com.mifos.service

import java.util.Collection;

import org.springframework.stereotype.Service;

import com.mifos.domain.LoanRepaymentSchedule

@Service
class CollectionTransformationServiceImpl implements CollectionTransformationService<LoanRepaymentSchedule>{

	static final Integer DEFAULT_USER_ID = 1
	
	@Override
	public Collection<LoanRepaymentSchedule> transform(Collection<LoanRepaymentSchedule> col) {
		
		List<LoanRepaymentSchedule> origin = new ArrayList<LoanRepaymentSchedule>(col);
		
		for(Integer i = 0; i < origin.size(); i++){
			Calendar duedateCal = Calendar.getInstance()
			duedateCal.setTime(origin.get(i).duedate)
			duedateCal.add(Calendar.MONTH, 1)
			
			Date duedate = duedateCal.getTime()
			origin.get(i).duedate = duedate
			origin.get(i).lastmodifiedDate = new Date()
			origin.get(i).lastmodifiedbyId = DEFAULT_USER_ID
			
			Integer following = i+1
			if(following < origin.size()){
				origin.get(following).fromdate = duedate
			}
		}
		
		return origin;
	}

}

package com.mifos.service

interface CollectionFilterService<T> {

	/**
	 * Takes the original list and return the collection needed for bussines logic
	 * @param col
	 * @return
	 */
	Collection<T> filter(Collection<T> col)
}

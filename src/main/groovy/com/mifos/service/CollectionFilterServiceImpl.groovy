package com.mifos.service

import java.util.Collection;

import org.springframework.stereotype.Service;

import com.mifos.domain.LoanRepaymentSchedule
import com.mifos.repository.LoanRepaymentScheduleRepository;

@Service
class CollectionFilterServiceImpl implements CollectionFilterService<LoanRepaymentSchedule>{

	@Override
	public Collection<LoanRepaymentSchedule> filter(Collection<LoanRepaymentSchedule> col) {
		
		if(!col) return Collections.EMPTY_LIST;
		
		List<LoanRepaymentSchedule> origin = new ArrayList<LoanRepaymentSchedule>(col);
		
		for(Integer i = 0; i <= origin.size(); i++){
			Calendar duedate = Calendar.getInstance()
			duedate.setTime(origin.get(i).duedate)
			if(duedate.get(Calendar.MONTH) == Calendar.DECEMBER){
				return origin
			}
			origin.remove(i)
			return filter(origin)
		}
		
		return Collections.EMPTY_LIST;
	}

}

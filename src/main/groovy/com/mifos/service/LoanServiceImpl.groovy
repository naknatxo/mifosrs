package com.mifos.service

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mifos.domain.LoanRepaymentSchedule

@Service
class LoanServiceImpl implements LoanService{

	@Autowired
	LoanRepaymentScheduleService loanRepaymentScheduleService
	
	@Autowired
	CollectionFilterService collectionFilterService
	
	@Autowired
	CollectionTransformationService collectionTransformationService
	
	@Override
	public Integer filterAndTransform(Long loanId) {

		Integer result = 0;
		
		// Get from ddbb
		Collection<LoanRepaymentSchedule> data =  loanRepaymentScheduleService.findByLoanIdOrderByFromdate(loanId)
		
		while(data){
			
			// Return list from fisrt duedate on december
			data = collectionFilterService.filter(data)
			
			// Do the bussines logic (changing due and from dates)
			data = collectionTransformationService.transform(data)
			
			result += data.size()
			//save data in ddbb
			data = loanRepaymentScheduleService.save(data)
			
		}
		
		return result;
	}

}

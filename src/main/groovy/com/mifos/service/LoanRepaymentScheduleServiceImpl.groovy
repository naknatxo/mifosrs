package com.mifos.service

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mifos.domain.LoanRepaymentSchedule
import com.mifos.repository.LoanRepaymentScheduleRepository;

@Service
class LoanRepaymentScheduleServiceImpl implements LoanRepaymentScheduleService<LoanRepaymentSchedule>{

	@Autowired
	LoanRepaymentScheduleRepository repo


	@Override
	public Collection<LoanRepaymentSchedule> findByLoanIdOrderByFromdate(Long loanId) {
		
		if(loanId && loanId > 0L){
			return repo.findByLoanIdOrderByFromdate(loanId)
		}
		return Collections.EMPTY_LIST;
	}	

	@Override
	public Collection<LoanRepaymentSchedule> save(Collection<LoanRepaymentSchedule> data) {
		
		if(data){
			return repo.save(data)
		}
		return Collections.EMPTY_LIST;
	}
	
	
}

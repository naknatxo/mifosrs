package com.mifos.service

import java.util.Collection;

/**
 * This interface interacts with ddbb
 * @author nak
 *
 * @param <T>
 */
interface LoanRepaymentScheduleService<T> {
	
	/**
	 * Get data base collection from a given id
	 * @param id
	 * @return
	 */
	Collection<T> findByLoanIdOrderByFromdate(Long id)
	
	/**
	 * Save collection data
	 * @param data
	 * @return
	 */
	Collection<T> save(Collection<T> data)
}

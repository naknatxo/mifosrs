package com.mifos.mvc

import java.util.Collection;

import groovy.util.logging.Log4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ModelAttribute

import com.mifos.domain.LoanRepaymentSchedule;
import com.mifos.service.LoanRepaymentScheduleService;
import com.mifos.service.LoanService;

@Log4j
@RestController
@RequestMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
class LoanRepaymentScheduleController {

	@Autowired
	LoanService loanService
	
	@Autowired
	LoanRepaymentScheduleService loanRepaymentScheduleService
	
	@RequestMapping(value='${mifos_rs.rest.fix_loan_schedule}', method=RequestMethod.PUT)
	public  ResponseEntity<Map> setDates(@PathVariable Long id) {
		
	
		log.debug "********* adjusting:: ${id}"

		try {
			new ResponseEntity<Map>(["result":"Made "+loanService.filterAndTransform(id)+" changes"], HttpStatus.OK)
		} catch (Exception e) {
			e.printStackTrace()
			new ResponseEntity<Collection<LoanRepaymentSchedule>>(null, HttpStatus.INTERNAL_SERVER_ERROR)
		}
		
	}
	
	
	/***********Test Methods***************/
	
	@RequestMapping(value='${mifos_rs.rest.test_ws_up}', method=RequestMethod.GET)
	public  ResponseEntity<Map> test(@RequestParam(value = "requestParam", required = false) String requestParam) {
		
	
		log.debug "********* test zone:: ${requestParam}"

		new ResponseEntity<Map>(["resp":"${requestParam} in Test Area"], HttpStatus.OK)
	}

	@RequestMapping(value="/findByLoanId", method=RequestMethod.GET)
	public  ResponseEntity<Collection<LoanRepaymentSchedule>> findByLoanId(@RequestParam(value = "loanId", required = false) Long loanId) {
		
	
		log.debug "********* test zone:: ${loanId}"

		new ResponseEntity<Collection<LoanRepaymentSchedule>>(loanRepaymentScheduleService.findByLoanIdOrderByFromdate(loanId), 
																													   HttpStatus.OK)
	}
	
}

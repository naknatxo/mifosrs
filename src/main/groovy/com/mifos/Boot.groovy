package com.mifos

import org.springframework.boot.SpringApplication
import com.mifos.config.Application

class Boot {

	public static void main(String[] args) {
		def context = SpringApplication.run(Application.class, args)
		println context
	}
}

package com.mifos.event

import java.util.Collection;
import java.util.Date;

import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

import com.mifos.domain.LoanRepaymentSchedule;

@Component
@RepositoryEventHandler(LoanRepaymentSchedule.class)
class LoanRepaymentScheduleEventHandler {

	static final Integer DEFAULT_USER_ID = 1
	
	@HandleBeforeSave
	void hanldeLoanRepaymentScheduleSave(LoanRepaymentSchedule resource){
		println '+++++++++++++ handle'
		resource.lastmodifiedDate = new Date()
		resource.lastmodifiedbyId = 9L //DEFAULT_USER_ID	
	}
	
}

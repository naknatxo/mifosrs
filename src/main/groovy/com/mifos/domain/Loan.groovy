package com.mifos.domain

import javax.persistence.Table
import java.io.Serializable
import java.util.HashSet
import java.util.Set

import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToMany
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

import com.fasterxml.jackson.annotation.JsonProperty

@Entity
@Table(name="m_loan")
class Loan {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	Long id

	@NotNull
	@Size(min=1)
	@Column(name="account_no", nullable = false)
	String accountNo
	
	//etc...
}

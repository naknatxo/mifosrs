package com.mifos.domain

import javax.persistence.Table

import java.io.Serializable
import java.util.HashSet
import java.util.Set

import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToMany
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

import com.fasterxml.jackson.annotation.JsonProperty

@ToString(
	includes = ["id", "loan_id", "fromdate","duedate", "installment"],
	includePackage = false,
	includeNames = true
)
@EqualsAndHashCode(
	includes = ["id"]
)
@Entity
@Table(name="m_loan_repayment_schedule")
class LoanRepaymentSchedule {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	Long id

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "loan_id", nullable = false)
	Loan loan
	
	@Column(name = "fromdate")
	Date fromdate
	
	@Column(name = "duedate", nullable = false)
	Date duedate
	
	@Column(name = "installment", nullable = false)
	Integer installment
	
	@Column(name = "lastmodified_date")
	Date lastmodifiedDate
	
	@Column(name = "lastmodifiedby_id", nullable = false)
	Integer lastmodifiedbyId
	
	// Date lastmodified_date    Long lastmodifiedby_id
}

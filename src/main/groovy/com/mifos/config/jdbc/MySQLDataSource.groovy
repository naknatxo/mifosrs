package com.mifos.config.jdbc

import com.mifos.config.jdbc.hikari.HikariDataSourceDefinition;
import com.mifos.config.jdbc.hikari.HikariPoolDefinition;
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Scope

import javax.annotation.Resource
import javax.sql.DataSource

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile

@Configuration
@ComponentScan(basePackageClasses = [HikariDataSourceDefinition])
@Profile(["mysql","mysql-local"])
class MySQLDataSource {

	@Resource
	HikariPoolDefinition hikariPoolDefinition

	@Resource
	HikariDataSourceDefinition hikariDataSourceDefinition

	@Bean
	@Scope()
	DataSource dataSource() {
		new HikariDataSource(
				hikariConfig()
		)
	}

	@Bean
	HikariConfig hikariConfig() {
		// ugly hack to convert @ConfigurationProperties files (type-safe, refresh scope) into Property files
		// required by HikariConfig
		def config = [:]
		hikariDataSourceDefinition.properties.each { k, v ->
			if (k != "class") {
				config.put("dataSource.${k}", v)
			}
		}
		hikariPoolDefinition.properties.each { k, v ->
			if (k != "class") {
				config.put(k, v)
			}
		}

		def properties = new Properties()
		properties.putAll(config)

		new HikariConfig(properties)
	}

}

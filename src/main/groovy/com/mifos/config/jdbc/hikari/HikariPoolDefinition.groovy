package com.mifos.config.jdbc.hikari

import groovy.transform.CompileStatic
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@CompileStatic
@Component
@ConfigurationProperties(
		prefix = "mifos_rs.jdbc",
		ignoreNestedProperties = true
)
class HikariPoolDefinition {

	String dataSourceClassName
	String poolName
	String connectionTestQuery
	Integer maximumPoolSize
	Integer idleTimeout

}

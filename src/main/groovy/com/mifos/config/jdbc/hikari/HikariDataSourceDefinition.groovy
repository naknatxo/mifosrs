package com.mifos.config.jdbc.hikari

import groovy.transform.CompileStatic
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@CompileStatic
@Component
@ConfigurationProperties(
		prefix = "mifos_rs.jdbc.dataSource"
)
class HikariDataSourceDefinition {
	
	String url
	String user
	String password
	Boolean cachePrepStmts
	Integer prepStmtCacheSize
	Integer prepStmtCacheSqlLimit
	Boolean useServerPrepStmts
}

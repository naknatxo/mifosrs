package com.mifos.config

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.orm.jpa.EntityScan
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc

import com.mifos.config.jdbc.HSQLDataSource;
import com.mifos.config.jdbc.MySQLDataSource;
import com.mifos.config.populate.Populate;
import com.mifos.domain.Loan;
import com.mifos.event.LoanRepaymentScheduleEventHandler;
import com.mifos.mvc.LoanRepaymentScheduleController;
import com.mifos.repository.LoanRepository;
import com.mifos.service.CollectionFilterServiceImpl;

@Configuration
@Import([
		HSQLDataSource,
		MySQLDataSource,
		Populate
])
@EnableJpaRepositories(
	basePackageClasses = [LoanRepository]
)
@EntityScan(
	basePackageClasses = [Loan.class]
)
@ComponentScan(
	basePackageClasses = [
			CollectionFilterServiceImpl,
			LoanRepaymentScheduleEventHandler,
			LoanRepaymentScheduleController
		]
)
//@EnableWebMvc
@EnableTransactionManagement
@EnableAutoConfiguration
class Application {

}

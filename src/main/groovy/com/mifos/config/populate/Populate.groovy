package com.mifos.config.populate

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.joda.JodaModule
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.Resource
import org.springframework.data.repository.init.Jackson2RepositoryPopulatorFactoryBean

@Configuration
@Profile(["populate"])
class Populate {

	@Autowired ApplicationContext applicationContext
	
	@Bean
	public Jackson2RepositoryPopulatorFactoryBean repositoryPopulator() {

		Resource loans  = new ClassPathResource("data/loan.json")
		Resource loansRepaymentShedule  = new ClassPathResource("data/loanRepaymentSchedule.json")

		ObjectMapper mapper = new ObjectMapper()
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
		mapper.registerModule(new JodaModule()) // for joda date times

		Jackson2RepositoryPopulatorFactoryBean factory = new Jackson2RepositoryPopulatorFactoryBean()
		factory.setMapper(mapper)
		factory.setResources( [ loans, loansRepaymentShedule ] as Resource[] )

		return factory
	}
	
}
